Разработка примера базы данных для демонстрации возможностей Ред Базы данных (расчет заработных плат)                                                    
=====================================================================================================
Задача данной работы в учебных целях изучить возможности СУБД Ред База Данных 3.0 на основе ядра Firebird 3.0,
в котором реализованы новые возможности языка SQL. База данных должна включать основные компоненты с использованием
нововведений. Подходящей тематикой для создания базы данных является расчет заработной платы, так как это сложный процесс,
который позволяет отобразить достаточное количество возможностей СУБД и нововведений.
В рамках работы рассмотрен следующий перечень нововведений:

−	Оконные (аналитические) функции (Window (Analytical) Functions).
---------------------------------------------------------------------
>Согласно SQL спецификации оконные функции (также известные как аналитические функции) являются своего рода
>агрегатными функциями, не уменьшающими степень детализации. При этом агрегированные данные выводятся вместе
>с неагрегированными.

−	PSQL Хранимые функции (PSQL Stored Functions).
---------------------------------------------------------------------
>Хранимая функция является программой, хранящейся в области метаданных базы данных и выполняющейся на стороне
>сервера. В отличие от хранимых процедур хранимые функции всегда возвращают одно скалярное значение.

−	Пакеты (Packages).
---------------------------------------------------------------------
>Функции и процедуры теперь можно объединять в группы, называемые пакетами, которые
>будут представлять собой один объект базы данных.

−	DDL триггеры (DDL triggers).
---------------------------------------------------------------------
>DDL триггеры срабатывают на указанные события изменения метаданных в одной из фаз события.
>BEFORE триггеры запускаются до изменений в системных таблицах, AFTER триггеры запускаются после изменений
>в системных таблицах.

−	Управление пользователями с помощью SQL (SQL Features for Managing Access).
-------------------------------------------------------------------------------
>Нововведениями является включение и выключение пользователя без его удаления, а также изменение
>текущего пользователя с помощью ALTER CURRENT USER

−	Логический тип BOOLEAN (BOOLEAN Data Type).
---------------------------------------------------------------------
>Тип данных BOOLEAN (8 бит) включает различные значения истинности TRUE и FALSE. Если не установлено
>ограничение NOT NULL, то тип данных BOOLEAN поддерживает также значение истинности UNKNOWN как NULL значение. 

−	Тип столбца Identity (Identity Column Type).
---------------------------------------------------------------------
>Столбец идентификации представляет собой столбец, связанный с внутренним генератором последовательностей. 
>Его значение устанавливается автоматически каждый раз, когда оно не указано в операторе INSERT. 

−	Управление допустимостью NULL значений для доменов и столбцов (Manage Nullability in Domains and Columns).
--------------------------------------------------------------------------------------------------------------
>Допустимость NULL значений для доменов и столбцов ранее была невозможна, но теперь синтаксис ALTER поддерживает
>изменение допустимости NULL значений.

−	Дополнительные возможности оператора MERGE (Supplemental SQL 2008 Features for MERGE).
------------------------------------------------------------------------------------------
>Оператор MERGE производит слияние записей источника в целевую таблицу (или обновляемое представление).
>Теперь у него есть поддержка предложения DELETE, а также поддержка  множества  предложений  WHEN  MATCHED 
>и WHEN NOT MATCHED с дополнительными условиями и поддержка предложения RETURNING … INTO.

−	Операторы OFFSET и FETCH(SQL:2008-Compliant OFFSET and FETCH Clauses).
--------------------------------------------------------------------------
>Операторы OFFSET и FETCH являются совместимым эквивалентом предложениям FIRST/SKIP и альтернативой
>предложению ROWS. Предложение OFFSET указывает, какое количество строк необходимо пропустить.
>Предложение FETCH указывает, какое количество строк необходимо получить. 

−	Поддержка левых параметров у оператора WHERE (Support for Left-side Parameters in WHERE Clause).
----------------------------------------------------------------------------------------------------
>За счет поддержки левых параметров у оператора WHERE в новой версии СУБД конструкция WHERE...IN (SELECT...) больше
>не вызывает ошибку "тип данных параметра неизвестен".

−	Улучшения для глобальных временных таблиц (An Improvement for GTTs).
------------------------------------------------------------------------
>Глобальные временные таблицы теперь доступны для записи даже для транзакций только для чтения.
>Транзакция для чтения и записи доступна для записи данных как в ON COMMIT PRESERVE ROWS, так и в
>ON COMMIT DELETE ROWS. Транзакция только для чтения доступна для записи данных только в ON COMMIT DELETE ROWS.

−	Исключения с параметрами (Exceptions with parameters).
----------------------------------------------------------
>Исключение теперь в новой версии могут быть определены с сообщением, содержащим слоты для параметров,
>которые заполняются при возбуждении исключения.

−	Оператор CONTINUE (CONTINUE in Looping Logic).
---------------------------------------------------------------------
>CONTINUE – это оператор досрочного начала новой итерации цикла. Оператор CONTINUE моментально начинает новую
>итерацию внутреннего цикла операторов WHILE или FOR. С использованием опционального параметра метки LEAVE также
>может начинать новую итерацию для внешних циклов.

−	Ссылки на PSQL курсоры как на переменные (PSQL Cursors as Variables).
-------------------------------------------------------------------------
>Теперь в PSQL поддерживаются ссылки на курсоры, как на переменные типа запись. Текущая запись доступна через имя
>курсора для явных (DECLARE AS CURSOR) и неявных (FOR SELECT) PSQL курсоров, что делает необязательным предложение INTO. 
